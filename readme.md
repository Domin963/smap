# Projekt SMAP
## Rozpoznávání zvířat na leteckých snímcích


Tento projekt slouží k rozpoznávání zvířat na leteckých snímcích pomocí konvoluční neuronové sítě (CNN). 
Projekt vychází z:
 BAUER, A. et al. Combining computer vision and deep learning to enable ultra-scale aerial phenotyping and precision agriculture: A case study of lettuce production. Horticulture Research. 2019-12, vol. 6, no. 1, p. 70. DOI: 10.1038/s41438-019-0151-5. 

Dataset pochází z:
 Han, Liang & Tao, Pin & Martin, Ralph. (2019). Livestock detection in aerial images using a fully convolutional network. Computational Visual Media. 5. DOI: 10.1007/s41095-019-0132-5. 


## Struktura projektu

Projekt se skládá z několika částí:

**Datová část**
Před použitím projektu pro trénování či porovnávání je nutné rozbalit soubor _dataset.zip_. Tento soubor obsahuje složku dataset - snímky pro predikci, trénování, testování a porovnání. Pozor - souborů pro trénování je velmi mnoho (řádově desetitisíce).

**Vytvoření trénovacích a testovacích dat z datasetu**
- Soubor _create_training_dataset_part1_
Dataset se nachází ve složce dataset, každý obrázek má k sobě soubor .xml, který obsahuje bounding boxy označující jednotlivá zvířata na snímku. Výsledkem souboru _create_training_dataset_part1_ je složka, ve které jsou obrázky rozděleny do složek _positives_ a _negatives_.

**Trénink CNN**
- Soubor _cnn_train_model_part1_ nejdříve rozdělí data na testovací a trénovací a následně natrénuje model.
Výsledkem je model, který je uložen ve složce models, v kořenovém adresáři se nacházejí grafy Accuracy a Loss učení.

**Použití natrénované CNN pro predikci zvířat**
- Soubor _main_file_evaluation_  
V tomto souboru zvolíme model, který chceme použít - proměnná loaded_model, můžeme zvolit krok posunu - proměnná stride (default 30) a hranici pravděpodobnosti, že je zkoumaný prvek zvíře treshold (default 0,65).
Princip vyhodnocení obrazu: ze vstupního souboru postupně vytváříme čtverce o délce strany 250 px, a z těchto čtverců CNN předkládáme čtvercová "okna" velikosti 32 px. Proměnná stride určuje, o kolik px se tato "okna" posouvají. Pokud je pravděpodobnost, že je zkoumaný objekt na čtverci zvíře menší než treshold, tak se pokračuje v prohledávání, jinak je objekt označen.

Do složky input vložíme snímky, které chceme vyhodnotit. Do složky output se po spuštění souboru uloží výsledné snímky s označenými zvířaty. Ve složce output se také vytvoří složka files, která obsahuje soubory .npy s daty sloužícími pro označení zvířat - hlavní je soubor _boxes.npy_, který obsahuje souřadnice jednotlivých značek.

**Porovnání s ručně označenými zvířaty**
- Soubor _hand_count_comparison_  
Tento soubor slouží k porovnání výsledků dosažených modelem s ručně spočítanými výskyty zvířat na zvolených obrázcích. 
V případě nutnosti je možné přizpůsobit hodnotu tresh, která udává maximální Euklidovskou vzdálenost jednotlivých prvků tak, aby ještě byly prvky považovány za součást jednoho shluku.
